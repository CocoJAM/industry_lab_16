package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {
	protected Course course;
	protected DistributionPanel distributionPanel;

	public DistributionPanelAdapter(Course course , DistributionPanel distributionPanel) {
		this.course = course;
		this.distributionPanel = distributionPanel;
		course.addCourseListener(this);
	}

	@Override
	public void courseHasChanged(Course course) {
		distributionPanel.repaint();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */

}
