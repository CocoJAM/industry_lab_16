package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener{
	protected  Course course;
	protected StatisticsPanel statisticsPanel;

	public StatisticsPanelAdapter(Course course, StatisticsPanel statisticsPanel) {
		this.course = course;
		this.statisticsPanel = statisticsPanel;
		course.addCourseListener(this);
	}

	@Override
	public void courseHasChanged(Course course) {
		statisticsPanel.repaint();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
	
}
